package assignment6;
/**
 * @author Rafi Rashid
 * section# 16000
 */
/**
 * 
 * class that defines
 * a seat in the theater
 */
public class Seat {
	String seat_position;
	String seat_letter;
	int seat_number;
	boolean seat_taken;
	/**
	 * constructor for a seat given seat
	 * number, location, and letter
	 */
	public Seat(String letter, String location,int number)
	{
		seat_taken = false; //mark seat as not taken
		seat_number = number;
		seat_letter = letter;
		seat_position = location;
	}
}
