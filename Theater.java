package assignment6;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @author Rafi Rashid
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * class that represents
 * the theater with all the seats
 */
public class Theater implements Runnable
	{
	LinkedBlockingDeque<Seat> seats;
	/**
	 * constructor for a new theater
	 */
	public Theater(String theater_name)
	{
		seats = new LinkedBlockingDeque<>(); 
		if(theater_name.equals("Bates"))
		{
			this.createTheater();
		}
	}
	/**
	 * method that fills theater with seats
	 */
	public void createTheater()
	{
		//A
		for(int i = 108; i < 122; i++)
		{
			Seat s = new Seat("A", "Middle", i);
			seats.add(s);
		}
		for(int i = 122; i < 129; i++)
		{
			Seat s = new Seat("A", "House Right", i);
			seats.add(s);
		}
		//B
		for(int i = 108; i < 122; i++)
		{
			Seat s = new Seat("B", "Middle", i);
			seats.add(s);
		}
		for(int i = 122; i < 129; i++)
		{
			Seat s = new Seat("B", "House Right", i);
			seats.add(s);
		}
		//C
		for(int i = 101; i < 107; i++)
		{
			Seat s = new Seat("C", "House Left", i);
			seats.add(s);
		}
		for(int i = 108; i < 122; i++)
		{
			Seat s = new Seat("C", "Middle", i);
			seats.add(s);
		}
		for(int i = 122; i < 129; i++)
		{
			Seat s = new Seat("C", "House Right", i);
			seats.add(s);
		}
		//D-X
		for(char c ='D'; c < 'Y'; c++)
		{
			for(int i = 101; i < 108; i++)
			{
				Seat s = new Seat(Character.toString(c) , "House Left", i);
				seats.add(s);
			}
			for(int i = 108; i < 122; i++)
			{
				Seat s = new Seat(Character.toString(c), "Middle", i);
				seats.add(s);
			}
			for(int i = 122; i < 129; i++)
			{
				Seat s = new Seat(Character.toString(c), "House Right", i);
				seats.add(s);
			}		
		}
		//Y
		for(int i= 101; i < 108; i++)
		{
			Seat s = new Seat("Y", "House Left", i);
			seats.add(s);
		}
		
		for(int i = 122; i < 129; i++)
		{
			Seat s = new Seat("Y", "House Right", i);
			seats.add(s);
		}
		//Z
		for(int i= 101; i < 108; i++)
		{
			Seat s = new Seat("Z", "House Left", i);
			seats.add(s);
		}
		
		for(int i = 122; i < 129; i++)
		{
			Seat s = new Seat("Z", "House Right", i);
			seats.add(s);
		}
		//AA
		for(int i= 101; i < 105; i++)
		{
			Seat s = new Seat("AA", "House Left", i);
			seats.add(s);
		}
		for(int i = 116; i < 119; i++)
		{
			Seat s = new Seat("AA", "House Left", i);
			seats.add(s);
		}
		Seat last_1 = new Seat("AA", "House Right", 127);
		seats.add(last_1);
		Seat last_2 = new Seat("AA", "House Right", 127);
		seats.add(last_2);
	}
	/**
	 * @return Seat
	 * method that finds,removes, and returns a seat
	 */
	public Seat bestAvailableSeat()
	{
		Seat found_seat = seats.poll();
		return found_seat;
	}
	/**
	 *  method that prints given seat
	 * @param to_print
	 */
	public void printTicketSeat(Seat to_print)
	{
		if(to_print == null) System.out.println("HELLO");
		System.out.println("Office "+Thread.currentThread().getName()
				+ ": Reserved " + to_print.seat_position + ", "
				+to_print.seat_letter+ ", "+to_print.seat_number);
	}
	/**
	 * 
	 * @param to_mark
	 * marks passed seat as taken 
	 * by marking boolean seat_taken as true
	 */
	public Seat markAvailableSeat(Seat to_mark)
	{
		to_mark.seat_taken = true;
		return to_mark;
	}
	/**
	 * method that runs during thread
	 */
	public void run() {
		ArrayList<Client> clients = new ArrayList<>();
		// make a list of 1000 clients
		for (int i = 0; i < 1000; i++)
		{
			Client new_client = new Client();
			clients.add(new_client);
		}
		
		while(!seats.isEmpty())
		{
			Seat check = this.bestAvailableSeat();
			if(check != null)
			{
				check = markAvailableSeat(check);
				this.printTicketSeat(check);
			}
			clients.remove(0);
			clients.add(new Client());
			try
			{
				Thread.sleep(1);
			}
			catch(Exception e){}		
		}
		System.out.println("Office "+Thread.currentThread().getName()+ " theater is sold out!!!!");
	}

}
