package assignment4;
/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;
/**
 * 
 * @author Rafi
 * this class sets up, reads/generates input, and gets stats for experiments
 */
public abstract class Experiment {
	private static final Integer SEED = 15485863;
	int invalidCount = 0;
	int foundCount = 0;
	int notfoundCount = 0;
	long elapsedTime;
	double elapsedTimeSec;
	/**
	 * 
	 * @return ArrayList of integers with million random integers
	 * sets up random arraylist of million integers
	 */
	public ArrayList<Integer> setUp()
	{
		Random rand = new Random(SEED);
		ArrayList<Integer> new_list = new ArrayList<Integer>();
		for (int n = 0; n < 1000000; n++)
		{
			Integer rand_int = rand.nextInt(Integer.MAX_VALUE);
			new_list.add(rand_int);
		}
		return new_list;
	}
	/**
	 * generates numbers for input.txt file
	 */
	public void generateInput()
	{
		try
		{
			FileOutputStream file = new FileOutputStream("input.txt");
			Random rand = new Random();
			PrintStream checker = new PrintStream(file);
			for(int i = 0; i < 1000; i++)
			{
				int randomNumber = (rand.nextInt(Integer.MAX_VALUE)-((Integer.MAX_VALUE)/2));
				checker.println(randomNumber);
			}
			checker.close();
		}catch(IOException e)
		{
			System.err.println ("Unable to write to input.txt");
			System.exit(-1);
		}
		
	
	}
	/**
	 * 
	 * @param filename
	 * @return ArrayList of integers with all valid integers from input
	 * reads from generated input file (default)
	 */
	public ArrayList<Integer> readFile (String filename)
	{
		ArrayList<Integer> input_list = new ArrayList<>(); 
		try 
		{
			FileReader freader = new FileReader(filename);
			BufferedReader reader = new BufferedReader(freader);
			for (String s = reader.readLine(); s != null; s = reader.readLine()) 
			{ 
				Integer check = Integer.valueOf(s);
				if (check <= 0){invalidCount++;}else {input_list.add(check);}
			}
			reader.close(); //close input.txt
		} 
		catch (FileNotFoundException e) 
		{
			System.err.println ("Error: File not found. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		} 
        catch (IOException e) 
		{
			System.err.println ("Error: IO exception. Exiting...");
			e.printStackTrace();
			System.exit(-1);
		}
		catch(NumberFormatException e){invalidCount++;}
		return input_list;
	}
	/**
	 * 
	 * @param experiment_number
	 * gets stats from experiment (default)
	 */
	public void getStats()
	{
		System.out.println(this.getClass().getSimpleName());
		System.out.println("Number of found values: " + foundCount);
		System.out.println("Number of values not found: " + notfoundCount);
		System.out.println("Number of illegal values in input file: " + invalidCount);
		System.out.println("Time elapsed in nanoseconds: " + elapsedTime);
		System.out.println("Time elapsed in seconds: " + elapsedTimeSec);

	}
	
}
