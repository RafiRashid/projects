package assignment4;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * this class runs experiment 5
 */
public class Experiment5 extends Experiment{
	/**
	 * 
	 * @param to_check array to check for element
	 * @param toFind element to find using interpolation search
	 * @return the index of what is found, if not found returns a -1
	 * this method searches for element toFind in array to_check using
	 * interpolationSearch
	 */
	public int interpolationSearch(ArrayList<Integer> to_check, Integer toFind)
    {
    	int low = 0;
        int high = to_check.size() - 1;
        int mid;
        while (to_check.get(low) < toFind && to_check.get(high) >= toFind) 

        {
        	mid = low + (int)Math.ceil(((float)(toFind - to_check.get(low)) * (high-low)/(to_check.get(high)-to_check.get(low))));
        	if (to_check.get(mid) < toFind){ low = mid + 1; }
        	else if (to_check.get(mid) > toFind){ high = mid - 1; }
        	else return mid;
        }
        return -1;
    }    
	/**
	 * 
	 * @param input
	 * @param to_check
	 * this method searches using experiment 5s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> input, ArrayList<Integer> to_check)
	{
		Collections.sort(to_check);
		StopWatch watch = new StopWatch();
		for (int i = 0; i < input.size(); i++)
		{
			watch.start();
			int check = interpolationSearch(to_check, input.get(i));
			watch.stop();
			if (check!=-1)foundCount++;
			else notfoundCount++;
		}
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);
	}

}
