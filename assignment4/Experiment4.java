package assignment4;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * this class runs experiment 4
 */
public class Experiment4 extends Experiment {
	/**
	 * 
	 * @param input
	 * @param to_check
	 * this method searches using experiment 4s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> input, ArrayList<Integer> to_check)
	{
		StopWatch watch = new StopWatch();
		Collections.sort(to_check);
		for (int n = 0; n < input.size(); n++) //for each input
		{
			watch.start();
			int index = Collections.binarySearch(to_check, input.get(n));
			if(index >= 0){foundCount++;}
			else{notfoundCount++;}
			watch.stop();
		}
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);		
	}
	
}
