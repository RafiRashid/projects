package assignment4;


/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */

/**
 * 
 * @author Rafi
 *this class runs the main
 */
public class a4Driver {
	/**
	 * 
	 * @param args
	 * the main of assignment4
	 */
	public static void main(String args[])
	{
		//experiment 1
		Experiment1 exp1 = new Experiment1();
		exp1.generateInput();
		exp1.search(exp1.readFile("input.txt"), exp1.setUp());
		exp1.getStats();
		
		//experiment 2
		Experiment2 exp2 = new Experiment2();
		exp2.search(exp2.readFile("input.txt"), exp2.setUp());
		exp2.getStats();

		//experiment 3
		Experiment3 exp3 = new Experiment3();
		exp3.search(exp3.setUp());
		exp3.getStats();
		
		//experiment 4
		Experiment4 exp4 = new Experiment4();
		exp4.search(exp4.readFile("input.txt"), exp4.setUp());
		exp4.getStats();
		
		//experiment 5
		Experiment5 exp5 = new Experiment5();
		exp5.search(exp5.readFile("input.txt"), exp5.setUp());
		exp5.getStats();
		
		//experiment 6
		Experiment6 exp6 = new Experiment6();
		exp6.search(exp6.readFile("input.txt"),exp6.setUp());
		exp6.getStats();
		
	}
}
