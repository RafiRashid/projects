package assignment4;

import java.util.ArrayList;
import java.util.HashMap;


public class Experiment6 extends Experiment {
	/**
	 * 
	 * @param input
	 * @param to_check
	 * this method searches using experiment 6s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> input, ArrayList<Integer> to_check)
	{
		HashMap<Integer, Integer> new_map = new HashMap<Integer,Integer>(to_check.size(), 0.5f);
		for (Integer i: to_check) {new_map.put(i, 0 );}
		StopWatch watch = new StopWatch();
		
		for(int i = 0; i < input.size(); i++)
		{
			watch.start();
			if(new_map.containsKey(input.get(i))){foundCount++;}
			else{notfoundCount++;}
			watch.stop();
		}
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);
	}
}
