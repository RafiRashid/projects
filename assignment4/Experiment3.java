package assignment4;

import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * this class runs experiment 2
 */
public class Experiment3 extends Experiment {
	/**
	 * 
	 * @param to_check
	 * this method searches using experiment 3s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> to_check)
	{
		StopWatch watch = new StopWatch();
		watch.start();
		Collections.sort(to_check);
		watch.stop();
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);
	}
	public void getStats()
	{
		System.out.println(this.getClass().getSimpleName());
		System.out.println("Time elapsed in nanoseconds: " + elapsedTime);
		System.out.println("Time elapsed in seconds: " + elapsedTimeSec);
	}
}
