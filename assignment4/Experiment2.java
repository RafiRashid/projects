package assignment4;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * this class runs experiment 2
 */
public class Experiment2 extends Experiment{
	/**
	 * 
	 * @param input
	 * @param to_check
	 * this method searches using experiment 2s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> input, ArrayList<Integer> to_check)
	{
		LinkedList<Integer> list = new LinkedList<>();
		list.addAll(to_check);
		StopWatch watch = new StopWatch();
		for (int n = 0; n < input.size(); n++) //for each input
		{
			watch.start();
			if(list.contains(input.get(n))){foundCount++;}
			else{notfoundCount++;}
			watch.stop();
		}
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);
	}
}
