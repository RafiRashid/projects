package assignment4;
/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * section# 16000
 */
import java.util.ArrayList;
/**
 * 
 * @author Rafi
 * this class runs Experiment 1
 */
public class Experiment1 extends Experiment{
	/**
	 * 
	 * @param input
	 * @param to_check
	 * this method searches using experiment 1s technique,
	 * and sets foundCount, notfoundCount, and elapsed time using
	 * the stopwatch class
	 */
	public void search(ArrayList<Integer> input, ArrayList<Integer> to_check)
	{
		StopWatch watch = new StopWatch();
		for (int n = 0; n < input.size(); n++) //for each input
		{
			watch.start();
			if(to_check.contains(input.get(n))){foundCount++;}
			else{notfoundCount++;}
			watch.stop();
		}
		elapsedTime = watch.getElapsedTime();
		elapsedTimeSec = (double)(elapsedTime*1.0e-9);
	}

}
