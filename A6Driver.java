package assignment6;
/**
 * @author Rafi Rashid
 * section# 16000
 */
/**
 * 
 * @author Rafi
 * 
 */
public class A6Driver {
	public static void main(String[] args)
	{
		Theater bates = new Theater("Bates");
		Thread box_officeA = new Thread(bates, "A");
		Thread box_officeB = new Thread(bates, "B");
		Thread box_officeC = new Thread(bates, "C");
		
		box_officeA.start();
		box_officeB.start();
		box_officeC.start();

	}
}
