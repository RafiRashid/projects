package assignment3;

/**
 * 
 * @author Rafi Rashid
 * @author Evan Gulick
 */
/**
 * 
 * Exception with int type indicator for specifying 
 * error type: length, color mismatch, or duplicate guess
 *
 */
public class IllegalGuessException extends Exception {
	private static final long serialVersionUID = 1L;
	//sets guess exception type
	private int type;
	public IllegalGuessException(int set)	
	{
		type = set;
	}
	public int getType()
	{
		return type;
	}

}
