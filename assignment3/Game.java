package assignment3;
import java.util.ArrayList; 

import javax.swing.JOptionPane;

/**
 * @author Rafi Rashid
 * @author Evan Gulick
 */
/**
 * 
 * The Game class has three methods:
 * 1) Start, which intializes the game, 
 * 2) run, which acts as the game engine (connected to the main A2 driver)
 * 3) win, which is called when the player has won, and 
 * 4) lose, which is called when the player has lost.
 */
public class Game 
{
	private static final int PEGS_TO_WIN = 5;
	private static final int NUM_GUESSES = 15;
	/**
	 * prints text ruless and asks the player if they want to play
	 * @param nothing
	 * @return nothing
	 */
	public void Start() 
	{
		System.out.println("Welcome to Mastermind. Here are the rules:\n"
		+ "This is a text version of the classic board game Mastermind.\n"
		+ "The computer will think of a secret code.\n"
		+ "The code consists of 5 colored pegs.\n"
		+ "The pegs MUST be one of seven colors:\n"
		+ "blue, green, orange, purple, red, yellow, or maroon.\n"
		+ "A color may appear more than once in the code.\n"
		+ "You try to guess what colored pegs are in the code and what order they are in.\n"
		+ "After you make a valid guess the result (feedback) will be displayed.\n"
		+ "The result consists of a black peg for each peg you have guessed\n"
		+ "exactly correct (color and position) in your guess.\n"
		+ "For each peg in the guess that is the correct color,\n"
		+ "but is out of position, you get a white peg.\n"
		+ "For each peg, which is fully incorrect, you get no feedback.\n"
		+ "Only the first letter of the color is displayed.\n"
		+ "B for Blue, R for Red, and so forth.\n"
		+ "When entering guesses you only need to\n"
		+ "enter the first character of each color as a capital letter.\n"
		+ "You have 15 guesses to figure out the secret code or you lose the game.");
		Object[] options = {"No thanks", "Yes please"};
		int check = JOptionPane.showOptionDialog(null,
		"Would you like to play Mastermind?",
		"Wanna play?",
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE,
		null,
		options,
		options[1]);
		if( check == 0 || check == -1)
		{ 
			JOptionPane.showMessageDialog(null, "We're sorry you didn't want to play!","OKAY!", JOptionPane.PLAIN_MESSAGE);
			System.exit(-1);
		}
	}    
	/**
	 * runs a 15 guess game given a player and a board object
	 * @pre assumes board and player are two initialized classes
	 * @post runs a 15 guess game with the two objects
	 * @param board, an initialized board class
	 * @param player, an initialized player class
	 * @param test_flag, indicator passed to generate code to determine 
	 * whether to print the generated secret code
	 */
	public void Run(Board board,Player player, Boolean test_flag)
	{
		board.generateCode(test_flag);
		board.guesses = new ArrayList<Guess>();
		int guess_counter = NUM_GUESSES;
		while(guess_counter > 0)
		{
			Guess new_guess = player.getInput(board.guesses); //get a valid guess/or history request
			if (new_guess.colors.equals("history")){board.showHistory();}
			else //is a guess...
			{
				new_guess = board.setPegs(new_guess); 
				board.guesses.add(new_guess);
				//print out the guess they submitted + "->Result: " and the resulting pegs
				JOptionPane.showMessageDialog(null, new_guess.colors + " -> " + new_guess.white_pegs 
				+" white pegs and " + new_guess.black_pegs + " black pegs.","Result!", JOptionPane.PLAIN_MESSAGE);
				if (new_guess.black_pegs == PEGS_TO_WIN){Win(test_flag);}	
				guess_counter --;
			}
		}
		//if you run out of guesses, you...
		Lose(test_flag);
	}
	/**
	 * is called when game is won, prompts to play again 
	 *takes input of test flag to play a new game in testing mode
	 * @param test_flag, indicator to determine whether to
	 * print the secret code for the next game if in testing mode
	 * @return nothing
	 */
		public void Win(Boolean test_flag)
	{
		JOptionPane.showMessageDialog(null, "Congratulations, YOU WIN!","WOW!", JOptionPane.PLAIN_MESSAGE);

		Object[] options = {"No thanks", "Yes please"};
		int check = JOptionPane.showOptionDialog(null,
		"Would you like to play Mastermind again?",
		"Wanna play again?",
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE,
		null,
		options,
		options[1]);
		if(check == 0) //player chose to quit
		{
			JOptionPane.showMessageDialog(null, "We're sorry you didn't want to play again!",
			"OKAY!",JOptionPane.PLAIN_MESSAGE);
			System.exit(-1);
		} 
		//initialize and run a new game
		Board new_board = new Board();
		Player new_player = new Player();
		Run(new_board, new_player, test_flag);
			 
	
		
		
	}
	/**
	 * method that is called when game is lost, prompts to play again
	 * 	takes input of test flag to play a new game in testing mode
	 * @param test_flag
	 * @return nothing
	 */
	public void Lose(Boolean test_flag)
	{
		JOptionPane.showMessageDialog(null, "Sorry you're out of guesses! You LOSE!","WOW!", JOptionPane.PLAIN_MESSAGE);
		
		Object[] options = {"No thanks", "Yes please"};
		int check = JOptionPane.showOptionDialog(null,
		"Would you like to play Mastermind again?",
		"Wanna play again?",
		JOptionPane.YES_NO_OPTION,
		JOptionPane.QUESTION_MESSAGE,
		null,
		options,
		options[1]);
		if(check == 0) //player chose to quit
		{
			JOptionPane.showMessageDialog(null, "We're sorry you didn't want to play again!",
			"OKAY!",JOptionPane.PLAIN_MESSAGE);
			System.exit(-1);
		} 
		//initialize and run a new game
		Board new_board = new Board();
		Player new_player = new Player();
		Run(new_board, new_player, test_flag);
			 
	}
}
