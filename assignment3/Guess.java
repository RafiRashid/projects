package assignment3;

/**
 * 
 * @author Rafi Rashid
 * @author Evan Gulick
 */
/**
 * 
 * The Guess class stores the String of colors that a player guesses
 * It is passed to the guess checker, which returns the guess with the
 * correct number of white and black pegs.
 */
public class Guess {
	public String colors;
	public int white_pegs = 0;
	public int black_pegs = 0;
}
