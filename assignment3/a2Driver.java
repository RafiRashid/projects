package assignment3;
/**
 * 
 * @author Rafi Rashid, EID: rsr873 
 * @author Evan Gulick, EID: eg22492
 * section# 16000
 */

/**
 * This is the driver for the game; it is the game engine
 */
public class a2Driver 
{
	/**
	 * @param args
	 * @return nothing
	 */
	public static void main (String args[])
	{ 
		Boolean test_flag = true; 
		Game first_game = new Game(); 
		Player game_player = new Player(); 
		Board game_board = new Board(); 
		first_game.Start(); //print starting text, start first game
		first_game.Run(game_board,game_player,test_flag); //runs the game loop given a player and a board
	}

}
