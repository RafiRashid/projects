package assignment3;

import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.Random;
/**
 * 
 * @author Rafi Rashid
 * @author Evan Gulick
 * 
 */
 
/**
  * 
 *The board class is the "computer player" of the game
 *It creates and holds the secret code, and stores and controls the guess history
 *
 */
public class Board 
{
	private static final int CODE_LENGTH = 5;
	private static final int NUM_COLORS = 7;
	private static final String COLORS = "BGOPRYM";
	public String secretCode = "";
	public ArrayList<Guess> guesses;
	/**
	 * method that generates the secret code
	 * @param test_flag to determine whether to print code for testing
	 * @return nothing
	 */
	public void generateCode(Boolean test_flag) 
	{
		JOptionPane.showMessageDialog(null, "Generating secret code...",
		"CALCULATING...",JOptionPane.PLAIN_MESSAGE);		for (int i = 0; i < CODE_LENGTH; i++)
		{
			Random rand = new Random();
			int rand_int = rand.nextInt(NUM_COLORS);
			secretCode += COLORS.charAt(rand_int);
		}
		if(test_flag){System.out.println(secretCode);}
	}
	/**
	 * @pre assumes a valid guess
	 * @post returns guess with pegs added,checks against secret code
	 * @param playerGuess, a Guess class with colors set
	 * @return Guess, a Guess class with colors and pegs set
	 */
	public Guess setPegs(Guess playerGuess)
	{
		int whitepegs = 0;
		int blackpegs = 0;
		StringBuilder tempanswer = new StringBuilder(secretCode);
		StringBuilder tempguess = new StringBuilder(playerGuess.colors);
		for(int k = 0; k<tempguess.length();k++)
		{
			if(tempguess.charAt(k) == tempanswer.charAt(k))
			{
				blackpegs++;
				tempguess.deleteCharAt(k);
				tempanswer.deleteCharAt(k);
				k--;
			}
		}
		for(int k = 0; k<tempguess.length();k++)
		{
			for(int j = 0;j<tempanswer.length();j++)
			{
				if(tempguess.charAt(k) == tempanswer.charAt(j))
				{
					whitepegs++;
					tempguess.deleteCharAt(k);
					tempanswer.deleteCharAt(j);
					k--;
					j=tempanswer.length();
				}
			}
		}
		playerGuess.black_pegs += blackpegs;
		playerGuess.white_pegs +=  whitepegs;
		return playerGuess;
	}
	/**
	 * shows the history of guesses
	 * @param nothing
	 * @return nothing
	 */
	public void showHistory() 
	{
		if (guesses.size() == 0)
		{ 
			JOptionPane.showMessageDialog(null,"oh no, you have no history, you haven't even played!","Error!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		ArrayList<String> optionList = new ArrayList<String>();
		String output = "";
		for(int i = 0; i < guesses.size(); i += 1)
		{
			output = "";
			output += i+1 + " -> ";
			output += "Your Guess: " + guesses.get(i).colors + ". Resulting Pegs...";
			output += " Black: " +guesses.get(i).black_pegs + " White: " + guesses.get(i).white_pegs;
			optionList.add(output);
		}
		Object[] options = optionList.toArray();
		JOptionPane.showInputDialog(null, 
        "A Drop Down of Your Guesses!", 
        "History!", 
         JOptionPane.PLAIN_MESSAGE, 
         null,
         options, 
         options[0]);		
	}
	
	
}
