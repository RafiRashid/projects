package assignment3;

import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * 
 * @author Rafi Rashid 
 * @author Evan Gulick
 */

/**
 * The Player class controls the input functionality and serves as the "player" of the game
 * The player can input either a valid guess or "history" to check the history of their guesses
 *
 */
public class Player {
	
	private static final int CODE_LENGTH = 5;
	private static final String COLORS = "BGOPRYM";
	private static final int NUM_GUESSES = 15;
	/**
	 * checks for errors and throws exceptions if errors are found
	 * @pre assumes that guesses contains initialized guesses to check with,
	 * assumes input_string is an inputted collected guess from user which may or may not be valid
	 * @post guess is now either valid or an exception has been thrown
	 * @param input_string collected string inputted from the user
	 * @param guesses is the arraylist of guess classes to check against
	 * @throws IllegalGuessException, indicative of an error whilst finding a legitimate guess
	 * @return nothing
	 */
	public void checkErrors(String input_string, ArrayList<Guess> guesses) throws IllegalGuessException
	{
		//is a guess
		if (input_string.length() != CODE_LENGTH)
		{
			throw new IllegalGuessException(1); 
		}
		
		String check = COLORS;
		for (int i = 0; i < CODE_LENGTH; i++)
		{	
			if(	check.indexOf(input_string.charAt(i)) == -1)
			{
				throw new IllegalGuessException(2); 
			}
		}
		for (int i = 0; i < guesses.size(); i++)
		{
			if (input_string.equals(guesses.get(i).colors)){throw new IllegalGuessException(3);}
		}	
	}
	/**
	 * gets input from player and returns a guess with colors set
	 * @pre assumes that guesses contains validated guess classes
	 * and that the user will input something into joptionpane window
	 * @post will return a valid, checked guess with colors set
	 * @param guesses, the arraylist of guess classes
	 * @return Guess
	 */
	public Guess getInput(ArrayList<Guess> guesses) 
	{
		Guess new_guess = new Guess();
		Boolean error_flag;
		while(true)
		{	
			error_flag = false;
			int curr_guess = NUM_GUESSES - guesses.size();
			String input_string = (String)JOptionPane.showInputDialog(
                    null,
                    "You have " + curr_guess + " guesses left.\nEnter guess: ",
                    "Input Guess or Check History!",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    null,
                    "Input guess or 'history' here");
			if(input_string == null) //player hit cancel/tried to exit
			{
				Object[] options = {"Yes please", "No thanks"};
				int check = JOptionPane.showOptionDialog(null,
				"Are you sure you want to exit?",
				"Really!?",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[1]);
				if(check == 1){continue;}//player chose to keep playing
				//else they chose to exit
				JOptionPane.showMessageDialog(null, "We're sorry you didn't want to keep playing!",
				"OKAY!",JOptionPane.PLAIN_MESSAGE);
				System.exit(-1);
				
			}
			try
			{
				if (input_string.equals("history"))
				{
					new_guess.colors = "history"; //set color to "history" if user wants to view history
				} else{checkErrors(input_string, guesses);}
			}
			catch(IllegalGuessException caught)
			{
				error_flag = true;
				switch(caught.getType())
				{
					case 1://wrong length case
						JOptionPane.showMessageDialog(null,"oh no, your guess is the wrong length!","Error!", JOptionPane.ERROR_MESSAGE);
						break;
					case 2://invalid color case
						JOptionPane.showMessageDialog(null,"oh no, your guess has an invalid color!","Error!", JOptionPane.ERROR_MESSAGE);
						break;
					case 3://duplicate case
						JOptionPane.showMessageDialog(null,"oh no, you've already guessed that dummy!","Error!", JOptionPane.ERROR_MESSAGE);
						break;

				}
			}
	
		if (!error_flag) {new_guess.colors = input_string; return new_guess;}
		}
	
	}
	
}
